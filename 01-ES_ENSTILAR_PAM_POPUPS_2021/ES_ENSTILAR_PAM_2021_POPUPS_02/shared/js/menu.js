var menuPrincipal;
var estructuraMenu;
var estructura;

function menu() {

    estructura = [];
    estructura[0] = ["ES_ENSTILAR_PAM_2021",  [
        "ES_ENSTILAR_PAM_2021_00",
        "ES_ENSTILAR_PAM_2021_01",
        "ES_ENSTILAR_PAM_2021_02",
        "ES_ENSTILAR_PAM_2021_03",
        "ES_ENSTILAR_PAM_2021_04",
        "ES_ENSTILAR_PAM_2021_05",
        "ES_ENSTILAR_PAM_2021_06",
        "ES_ENSTILAR_PAM_2021_07",
        "ES_ENSTILAR_PAM_2021_08",
        "ES_ENSTILAR_PAM_2021_09",
        "ES_ENSTILAR_PAM_2021_10",
        "ES_ENSTILAR_PAM_2021_11",
        "ES_ENSTILAR_PAM_2021_12",
        "ES_ENSTILAR_PAM_2021_13",
        "ES_ENSTILAR_PAM_2021_14",
        "ES_ENSTILAR_PAM_2021_15",
        "ES_ENSTILAR_PAM_2021_16",
        "ES_ENSTILAR_PAM_2021_17",
        "ES_ENSTILAR_PAM_2021_18",
        "ES_ENSTILAR_PAM_2021_19"
    ]];

    estructura[1] = ["ES_ENSTILAR_PAM_POPUPS_2021",  [
        "ES_ENSTILAR_PAM_2021_POPUPS_00",
        "ES_ENSTILAR_PAM_2021_POPUPS_01",
        "ES_ENSTILAR_PAM_2021_POPUPS_02",
        "ES_ENSTILAR_PAM_2021_POPUPS_03",
        "ES_ENSTILAR_PAM_2021_POPUPS_04",
        "ES_ENSTILAR_PAM_2021_POPUPS_05",
        "ES_ENSTILAR_PAM_2021_POPUPS_06",
        "ES_ENSTILAR_PAM_2021_POPUPS_07",
        "ES_ENSTILAR_PAM_2021_POPUPS_08",
        "ES_ENSTILAR_PAM_2021_POPUPS_09",
        "ES_ENSTILAR_PAM_2021_POPUPS_10"
    ]];

    if (is_develop) {
        edetailing.enableTestMode();
    }
    inicializar_navigate();
}