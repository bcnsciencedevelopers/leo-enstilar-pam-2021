var materials = [];
var buttonsSelected = [];

$(document).ready(function () {
  menu();
  main();
  espec();
});

function main() {
  // logo
  $("#global").append('<div class="logo_farma"></div>');

  $("#global").append('<div class="logo_prod_1" onclick="openPop(\'popFT\')"></div>');

  $("#global").append('<div class="btn-next js-goto-next"></div>');
  $("#global").append('<div class="btn-prev js-goto-prev"></div>');

  $("#global").append(
    '<div class="btn1 js-goto" data-slide="1" data-presentation="0"></div>'
  );
  $("#global").append(
    '<div class="btn2 js-goto" data-slide="2" data-presentation="0"></div>'
  );
  $("#global").append(
    '<div class="btn3 js-goto" data-slide="4" data-presentation="0"></div>'
  );
  $("#global").append(
    '<div class="btn4 js-goto" data-slide="6" data-presentation="0"></div>'
  );
  $("#global").append(
    '<div class="btn5 js-goto" data-slide="7" data-presentation="0"></div>'
  );
  $("#global").append(
    '<div class="btn6 js-goto" data-slide="16" data-presentation="0"></div>'
  );
  $("#global").append(
    '<div class="btn7 js-goto" data-slide="19" data-presentation="0"></div>'
  );

  // popups referencias
  $("#global").append(
    '<div class="btn_ref" onclick="openPop(\'popReferencias\')"></div>'
  );
  $("#global").append("<div id='popReferencias' class='popup'>");
  $("#global").append(
    "<div id='btn_close_popReferencias' class='btn-tancar' onclick='closePop(\"popReferencias\")'></div>"
  );

  if (contador_km < 7) {
  	$("#global").append("<img src='" + path_links + "shared/css/img/pop_ref.png' class='imgpopup' id='imgReferencias'>");
  } else {
	$("#global").append("<img src='" + path_links + "shared/css/img/pop_ref_2.png' class='imgpopup' id='imgReferencias'>");
  }

  $("#imgReferencias").appendTo("#popReferencias");
  $("#btn_close_popReferencias").appendTo("#popReferencias");


  $("#global").append("<div id='popFT' class='popup'><div class='ft_container'><img src='" + path_links +"shared/css/img/ft.png' alt='Ficha Técnica Enstilar'/></div></div>");
  $("#global").append("<div id='btn_close_popFT' class='btn-tancar' onclick='closePop(\"popFT\")'></div>");

  $("#btn_close_popFT").appendTo("#popFT");
}

/****************
 FUNCIONES
 ****************/
function openPop(id) {
  if ($(".btn-referencias").hasClass("disabled")) {
    return;
  }
  $("#" + id + ", " + "#imgPopGloss")
    .css("display", "block")
    .css("z-index", 9999);
  $("#" + id).animate(
    { opacity: 1 },
    edetailing.popupActual.dataTemps,
    function () {}
  );
  $("#overlay").addClass("active");
}

function closePop(id) {
  $("#" + id).css("z-index", -1);
  $("#" + id).animate(
    { opacity: 0 },
    edetailing.popupActual.dataTemps,
    function () {
      $("#" + id).css("display", "none");
    }
  );
  $("#overlay").removeClass("active");
  if (id === "popAE") {
    $("#line").html("");
  }
}

/**
 * Cuando clicas una parte del cuerpo, se cambia la img
 */
$(".img").on("click", function () {
  var id = $(this).attr("id");
  var lastChar = id[id.length - 1];
  var penultimateChar = id[id.length - 2];

  // Popup
  $("#" + id).attr("onclick", 'openPop("popBig")');
  $("#overlay").addClass("active");
  $("#overlay").css("z-index", "99");

  if ($.isNumeric(penultimateChar)) {
    $("#global").append(
      "<img src='css/img/casos_clinicos/1/img-" +
        penultimateChar +
        lastChar +
        "-big.png' class='imgpopupBig' id='popBig'>"
    );
  } else {
    $("#global").append(
      "<img src='css/img/casos_clinicos/1/img-" +
        lastChar +
        "-big.png' class='imgpopupBig' id='popBig'>"
    );
  }

  $("#popBig").on("click", function () {
    $("#overlay").css("z-index", "-199");
    $("#overlay").removeClass("active");
    $("#popBig").remove();
  });
});
