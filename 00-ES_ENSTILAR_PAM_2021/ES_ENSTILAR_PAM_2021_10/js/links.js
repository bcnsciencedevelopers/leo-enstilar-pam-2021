/******************************
 FINALIDAD:

 Declarar las rutas del material, tanto del css como del js.
 Nos olvidaremos de quitar el '../../' de cada slide a la hora de subir al saleforce.

 USO:
 Antes de empezar hay que poner 2 líneas de código en el html:

 1) En el <head>: <script src="js/links.js"></script>
 2) Justo antes del </body>: <script>create_links_js();</script>

 Finalmente, asegurarnos de especificar todas las rutas en el primer link.js para luego copiar/pegar como dios manda en cada KM!

 USO ESPECIAL:
 Si queremos añadir una librería en un slide en concreto (por ejemplo, swiper.js), lo aplicaremos en 'link.js' de dicho slide.
 ******************************/




/******************************
 VARIABLES GLOBALES
 ******************************/

//Creates the corresponding link path for Salesforce/Veeva or localhost environments
var is_develop = false;


var path_develop = "../../";
var path_prod = "";
var path_links = "";
if( is_develop ){ path_links = path_develop; }
else{ path_links = path_prod; }


// Aquí declaramos todas los links css que se van a usar en toda la app
document.head.innerHTML += '<link rel="stylesheet" href=' + path_links + 'shared/css/edetailing-lib.css>';
document.head.innerHTML += '<link rel="stylesheet" href=' + path_links + 'shared/css/jquery-ui.min.css>';
document.head.innerHTML += '<link rel="stylesheet" href=' + path_links + 'shared/css/swiper.min.css>';
document.head.innerHTML += '<link rel="stylesheet" href=' + path_links + 'shared/css/main.css>';
document.head.innerHTML += '<link rel="stylesheet" href="css/espec.css">';


/**
 * Funcion que se llama desde el html para printar los js
 */
function create_links_js() {
    // Aquí declaramos todas los links js que se van a usar en toda la app
    document.write('<script src=' + path_links + 'shared/js/libs/jquery-2.1.0.min.js></script>');
    document.write('<script src=' + path_links + 'shared/js/libs/jquery-ui.min.js></script>');
    document.write('<script src=' + path_links + 'shared/js/libs/veeva-library.js></script>');
    document.write('<script src=' + path_links + 'shared/js/libs/edetailings-lib.js></script>');
    document.write('<script src=' + path_links + 'shared/js/libs/swiper.min.js></script>');
    document.write('<script src=' + path_links + 'shared/js/menu.js></script>');
    document.write('<script src=' + path_links + 'shared/js/main.js></script>');
    document.write('<script src=js/espec.js></script>');
}